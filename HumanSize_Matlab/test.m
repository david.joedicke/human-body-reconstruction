clear all
close all

test = 0;
function val=setTest()
    test = 32;
    disp(test);
end
function runHumanSize()
    model_length = 190;
    OBJ=readObj('examples\consensus.obj');
    FV.vertices=OBJ.v;
    FV.faces=OBJ.f.v;
    figure, patch(FV,'facecolor',[1 0 0]);

    val = FV.vertices(:,2);
    min_val=abs(min(val));
    max_val=abs(max(val));
    size = min_val+max_val;
    ind  = zeros(1,length(val));
    start = 0;
    range = 0.02;
    for i = 1:length(val)
        if val(i) <= start +range && val(i) >= start-range
            ind(i) = 1;
        else
            ind(i)=0;
        end

    end
    ind = logical(ind');
    A = FV.vertices(ind,:);

    x = A(:,1);
    z= A(:,3);
    figure, plot(x,z,'o')
    elipse = fit_ellipse(x,z);

    t=-pi:0.01:pi;
    x=elipse.X0+elipse.a*cos(t);
    y=elipse.Y0+elipse.b*sin(t);
    hold on
    plot(x,y)

    lambda = (elipse.a - elipse.b)/(elipse.a + elipse.b);
    u = (elipse.a + elipse.b)* pi * (1+((3*lambda^2)/(10+sqrt(4-3*lambda^2))));
    u_in_cm=(u*model_length)/size;
    fprintf('abdominal girth: %f cm\n', u_in_cm);
end
