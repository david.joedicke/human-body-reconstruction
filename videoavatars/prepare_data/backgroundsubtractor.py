import numpy as np
import cv2
import os
import argparse

"""

This script extract masks from an input video.

Example:
$ python backgroundsubtractor.py dataset/subject/ video.mp4

"""

parser = argparse.ArgumentParser()
parser.add_argument('src_folder', type=str)
parser.add_argument('video_file', type=str)
args = parser.parse_args()

os.chdir(args.src_folder)
print(os.getcwd())
vidcap = cv2.VideoCapture(args.video_file)
if not os.path.exists('mask'):
    os.makedirs('mask')
os.chdir('mask')
success, img = vidcap.read()
count = 0
color = 'green'

if color == 'green':
    while success:
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, (44, 54, 63), (71, 255, 255))
        imask = mask > 0
        outp = np.zeros_like(img, np.uint8)
        outp[~imask] = 255
        filename = 'frame' + str(count).zfill(5) + '.png'
        cv2.imwrite(filename, outp)
        success, img = vidcap.read()
        count += 1
        print('Read frame: ', count)
