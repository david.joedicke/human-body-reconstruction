import h5py
import os
import scipy.misc

os.chdir(r'/home/mwimmer/Desktop/cov/human-body-reconstruction/people_snapshot_public/female-1-casual-1')

filename = 'masks.hdf5'
f = h5py.File(filename, 'r')
print("Keys: %s" % f.keys())
a_group_key = list(f.keys())[0]

data = list(f[a_group_key])

os.chdir('mask')

for i in range(0, len(data)):
    print(i)
    scipy.misc.imsave('outfile' + str(i).zfill(5) + '.png', data[i])